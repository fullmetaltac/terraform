provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "node" {
  name = "node:alpine"
}

resource "docker_container" "webapp" {
  image = "${docker_image.node.name}"
  name  = "webapp"

  command = ["npm", "start"]

  ports {
    external = 8080
    internal = 8080
    protocol = "tcp"
  }

  upload {
    content = "${file("package.json")}"
    file    = "./package.json"
  }

  upload {
    content = "${file("server.js")}"
    file    = "./server.js"
  }
}